import React from 'react';
import ProgressBar from '../../components/ProgressBar';
import './progress-bar-container.less';
import { Row, Col, Button, Select} from 'antd';
import axios from 'axios';

const Option = Select.Option;

/*Progress Bar Container*/
class ProgressBarContainer extends React.Component {

    // default value
    state = {"buttons":[10,46,-9,-19],"bars":[-61,20,67],"limit":100,"selectedProgressBarIndex":0};

    componentDidMount(){
       const options = {
           method: 'GET',
           url: 'http://pb-api.herokuapp.com/bars'
       };
        axios(options).then((response)=>{
            let {data} = response;
            data.bars = data.bars.map((defaultBarValue)=>{
                const displayWidth = this.calculateWidth(defaultBarValue, data.limit);
                const width = (displayWidth > 100)? 100 : displayWidth;
                return {
                    value: (defaultBarValue < 0)? 0 : defaultBarValue,
                    width: `${width}%`,
                    displayWidth: `${displayWidth}%`,
                    background: 'cyan'
                }
            });
            this.setState({bars: data.bars, buttons: data.buttons, limit: data.limit, selectedProgressBarIndex: 0});
        }).catch((error)=>{
            console.error(error);
        })

    }

    handleButtonClick = (buttonVal) => (event) => {
        const {bars, selectedProgressBarIndex, limit} = this.state;
        const selectedProgressBar = bars[selectedProgressBarIndex];

        const newValue = selectedProgressBar.value + buttonVal;
        selectedProgressBar.value = (newValue < 0) ? 0 : newValue;

        const newWidth = this.calculateWidth(selectedProgressBar.value, limit);

        selectedProgressBar.displayWidth = newWidth + '%';
        selectedProgressBar.width = ((newWidth > 100)? 100 : newWidth) + '%';
        selectedProgressBar.background = (newWidth > 100)? 'red' : 'cyan';

        this.setState({bars:bars});
    };

    handleChange = (value) => {
        this.setState({selectedProgressBarIndex: value});
    };

    calculateWidth(value, limit){
        if(value < 1){
            return 0;
        }
        return Math.floor((value/limit)*100);
    }

    render(){

        const ProgressBars = this.state.bars.map((barObj, index) => {
            return (
                <Row className="progress-bar-row" key={index}>
                    <Col span={8}></Col>
                    <Col span={8}>
                        <div className="progress-bar-div">
                            <span>{barObj.displayWidth}</span>
                            <ProgressBar width={barObj.width} background={barObj.background} />
                        </div>
                    </Col>
                    <Col span={8}></Col>
                </Row>
            )
        });

        return (
            <div>
                {ProgressBars}
                <Row className="progress-bar-row">
                    <Col span={8}></Col>
                    <Col span={8} className="progress-bar-div">
                        <Select className="progress-bar-select" defaultValue={0} onChange={this.handleChange}>
                            {this.state.bars.map((barDefaultValue, index)=>{
                                return (
                                    <Option key={index} value={index}>
                                        {`ProgressBar# ${index+1}`}
                                    </Option>
                                );
                            })}
                        </Select>
                        {this.state.buttons.map((buttonValue, index)=>{
                            return (
                                <Button key={index}
                                        className="button-style"
                                        onClick={this.handleButtonClick(buttonValue)}>{buttonValue}</Button>
                            );
                        })}
                    </Col>
                    <Col span={8}></Col>
                </Row>
            </div>
        )
    }
}

export default ProgressBarContainer;
