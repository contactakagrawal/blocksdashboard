import React from 'react';
import { BrowserRouter as Router, Route, Switch } from 'react-router-dom';
import ProgressBarContainer from './ProgressBarContainer';

const App = () => (
        <Router>
          <div>
            <Switch>
              <Route exact path="/" component={ProgressBarContainer} />
            </Switch>
          </div>
        </Router>
);

export default App;
