import React from 'react'
import { render } from 'react-dom'

import Loadable from 'react-loadable';

const Loading = () => <div>
  Loading...
</div>

const LoadableComponent = Loadable({
  loader: () => import('./containers/App.js'),
  loading: Loading
})

render(<LoadableComponent />, document.getElementById('root'))
