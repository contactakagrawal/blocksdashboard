# React clientside
> Client side focused react application boilerplate

## Url

http://localhost:3000/

## How to run in dev

```
    change the NODE_ENV=development in package.json start script
    npm install
    npm start
```

## How to run in prod

```
    change the NODE_ENV=production in package.json start script
    npm install
    npm run build
    npm start
```

## Technologies Used

```
    React 16
    React router 4
    Webpack 4
    AntD
    Axios
```