const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const merge = require('webpack-merge');
const webpack = require('webpack');
const CompressionPlugin = require('compression-webpack-plugin');
const common = require('./webpack.common');
const path = require('path');

module.exports = merge(common, {
  entry: ['./src/client/index.js'],
  output: {
    filename: 'bundle.js',
    path: path.resolve(__dirname, 'build'),
    publicPath: '/build/'
  },
  plugins: [
    new UglifyJSPlugin(),    
    new CompressionPlugin()
  ],
  mode: 'production'
});
